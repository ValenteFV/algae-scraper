from bs4 import BeautifulSoup
from settings_local import *

class Bing_results:
        """
        class that will take in a chrome driver open chrome and search for one keyword
        given to it on the country specified in country_code
        """



        def __init__ (self, driver, keyword, country_code="gb"):
                self.keyword = keyword
                self.country_code = country_code
                self.driver = driver
                self.page_limit = 20 #limit of pages, it jumps every 10, 50 being 5 pages


        def bing_messages(self, keyword, page_number):
                keyword_f = keyword.replace("+", " ")
                print ("-"*10)
                print ("Seach for: " + keyword)
                print ("Country Code: " + self.country_code)
                print ("Page Number: " + str(page_number))
                print ("-"*15)

        def get_links(self):

                links = []
                soup = BeautifulSoup(self.driver.page_source, "lxml")
                for link in soup.find_all("a"):
                        try:

                            if link["href"].startswith('http'):
                                    #print (link["href"]) # enable for troubleshoot purposes
                                    links.append(link["href"])
                        except KeyError:
                            pass
                return links
        


        def get_not_wanted_list(self):
            not_wanted_urls = open(NOT_WANTED_URLS_PATH, "r")
            not_wanted_list = [line.strip() for line in not_wanted_urls]
            return not_wanted_list

        def clean_list(self, links_per_keyword, not_wanted_list):
            clean_list = set(links_per_keyword) - set([url for url in links_per_keyword for key in not_wanted_list if key in url]) #Teo's Magic
            # for i in clean_list:
            #     print (i)
            return clean_list


        def results_list(self):
                links_per_keyword = []
                keyword = self.keyword.replace(" ", "+")
                for page_number in range(0, self.page_limit, 20): # if you want only 2 pages then leave 10 if you want more increase it in tens, eg 20, 30, 40 for 3, 4 and 5 pages
                        url = f"https://www.bing.com/search?q={self.keyword}&first={page_number}&cc={self.country_code}"
                        self.driver.get(url)
                        self.bing_messages(keyword, page_number)
                        links = self.get_links()
                        links_per_keyword.extend(links)
                links_per_keyword = self.clean_list(links_per_keyword, self.get_not_wanted_list())

                return links_per_keyword


if __name__ == '__main__':
    Bing_results()






