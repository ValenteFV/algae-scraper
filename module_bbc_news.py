import requests
from bs4 import BeautifulSoup
import json
from translate_module import Translate as translate
from settings_local import *

class BBC_news:

    def __init__(self, country_code):
        """
        country_code is the two uppercase letter code (E.g UK, US, MX)
        """
        self.country_code = country_code


    @property
    def country_code(self):
        return self._country_code

    @country_code.setter
    def country_code(self, cc):
        if not isinstance(cc, str):
            raise TypeError ("country_code must be a two letter string (e.g. UK, US, MX)")
        self._country_code = cc
        if len(cc) != 2:
            raise Exception ("country_code must be a two letter string (e.g. UK, US, MX)")
        if not cc.isupper():
            raise Exception ("country_code must be a two letter uppercase string (e.g. UK, US, MX)")


    def get_country_name(self):
        """
        looks up the name of the country in the COUNTRY_JSON_PATH
        """
        with open(COUNTRY_JSON_PATH, 'r', encoding='utf8') as f:
            country_file = json.load(f)    
        return (country_file[self.country_code.upper()]["name"])

    def format_country_name(self, country_name):
        country_name = self.get_country_name()
        country_name = country_name.replace(" ", "%20")
        return country_name

    def headlines(self):
        country_headlines = []
        country_name = self.get_country_name()
        f_country_name = self.format_country_name(country_name)
        print (f"Searching News in the BBC for: {country_name}")
        url = f"https://www.bbc.co.uk/search?q={f_country_name}"
        print (url)
        request = requests.get(url)
        html = (request.text)
        soup = BeautifulSoup(html, "lxml")
        for title in soup.find_all("h1",{"itemprop": "headline"}):
            try:
                country_headlines.append(title.text)
            except Exception as e:
                print (e)
        return country_headlines

    def get_language_code(self):
        """
        looks up the language from COUNTRY_JSON_PATH path specified in settings_local.py
        """
        with open(COUNTRY_JSON_PATH, 'r', encoding='utf8') as f:
            country_file = json.load(f)    
        return (country_file[self.country_code]["languages"][0])

    def translated_headlines(self):
        translated_headlines_list = []
        translator = translate()
        for headline in self.headlines():
             t_headline = translator.translate("en", self.get_language_code(), headline)
             translated_headlines_list.append(t_headline)
        return translated_headlines_list




# def main():
#     news = BBC_news("MX")
#     print(news.translated_headlines())


if __name__ == '__main__':
    BBC_news()