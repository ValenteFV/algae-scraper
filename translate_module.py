import requests
from settings_local import *


class Translate:
    """
    Simple translate class using Yandex api 
    https://tech.yandex.com/translate/doc/dg/reference/translate-docpage/#JSON
    api key can be obtained from the website currently using the one from 
    """
    def __init__(self):
        self.base_url = "https://translate.yandex.net/api/v1.5/tr.json/translate"
        self.api_key = TRANSLATE_API_KEY


    def translate(self, from_lang, to_lang, text):
        
        lang = from_lang + "-" + to_lang

        data = {"text": text,
        "format": "plain",
        "lang": lang,
        "key": self.api_key}

        try:
            response = requests.post(self.base_url, data=data)
            response = response.json()
            return (response["text"][0])
        except:
            raise Exception ("Error while getting response from: " + self.base_url)


# def main():
#     t = Translate()
#     print (t.translate("en", "es", "Hello World"))


if __name__ == "__main__":
    Translate()
