from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from bing_module import bing_module
from module_app_trends import app_trend_module
from available_countries import available_countries
from bbc_news import bbc_news_module
from datetime import datetime
import requests

def chrome_setup():
    CHROMEDRIVER_PATH = r"chromedriver" # in windows add .exe
    options = Options()
    options.add_argument('--headless')
    options.add_argument('--disable-gpu')  # Last I checked this was necessary.
    options.add_argument('--no-sandbox') #Also seems necessary
    prefs = {'profile.managed_default_content_settings.images':2}#so the browser doesnt load images
    options.add_experimental_option("prefs", prefs)#so the broswer doesn't load images
    driver = webdriver.Chrome(CHROMEDRIVER_PATH, chrome_options=options)
    return driver

def create_filename(country_code):
    date = f"{datetime.now():%Y_%m_%d}"
    file_name = f"{country_code}_{date}_MAINSTREAM.txt"
    return file_name

def write_urls(full_clean_list, country_code, file_name):
    file_name_path = "lists/" + file_name
    tag = file_name[:-4]
    tag_id = get_tag_id(tag)
    print ("Writing and Uploading URLS to URLDB.... This might take a while")
    with open(file_name_path, "a+",  encoding="utf-8") as txtfile:
            for url in full_clean_list:
                    txtfile.write(url)
                    api_call(url, tag_id)
                    txtfile.write('\n')

def write_exceptions(exception):
    date = f"{datetime.now():%Y_%m_%d_%H:%M:%S}"
    exception = str(exception)
    format_exception = f"{date}_{exception}"
    with open("exception.txt", "a+",  encoding="utf-8") as exception_txt:
                exception_txt.write(format_exception)
                exception_txt.write('\n')

def get_full_lists(keywords_list, driver, country_code):
    full_list = []
    for keyword in keywords_list:
            keyword_list = bing_module(driver, keyword, country_code)
            full_list.extend(keyword_list)
    return full_list     

def get_not_wanted_list():
    not_wanted_urls = open("not_wanted_urls.txt", "r")
    not_wanted_list = [line.strip() for line in not_wanted_urls]
    return not_wanted_list

def clean_list(links_per_keyword, not_wanted_list):
    clean_list = set(links_per_keyword) - set([url for url in links_per_keyword for key in not_wanted_list if key in url]) #Teo's Magic
    for i in clean_list:
        print (i)
    return clean_list

def main_json_filename():
    date = f"{datetime.now():%Y_%m_%d}"
    main_json_filename = f"{date}_main_json.json"
    return main_json_filename

def create_main_json(json_filename):
    
    main_json = open(json_filename, "w")
    main_json.close
    return json_filename

def create_dictionary(countries_list):
    main_dictionary = {}
    for country_code in countries_list:
        main_dictionary[country_code] = []
    #print (main_dictionary)
    return main_dictionary    
    
### ------------------------ set up  ---------------------------------------

def set_up(countries_list):
    """
    First part where dictionary is created and keywords are added
    """
    main_dictionary = create_dictionary(countries_list)
    #print (main_dictionary)
    for country_code in countries_list:
        try:
            main_dictionary = bbc_news_module(country_code, main_dictionary)
            main_dictionary = app_trend_module(country_code, main_dictionary)
            print ("-"*50)
        except Exception as exception:
            print (exception)
            write_exceptions(exception)
        finally:
            pass
    return main_dictionary

### ------------------------ Get Tag Ids ---------------------------------------

def search_tag(tag):
    """
    Checks if the tag exists on the database
    returns the id if it does or else it returns None
    """
    testurlstags = requests.get("http://urldb.empello.net/api/testurlstags.json").text
    testurlstags = eval(testurlstags)
    print (tag)
    for tag_dict in testurlstags:
        if tag_dict["name"] == tag:
            return tag_dict["id"]

def create_tag(tag):
    r = requests.post("http://urldb.empello.net/api/testurlstags/", data={'name':tag})
    tag_id = r.json()["id"]
    return tag_id
                
def get_tag_id(tag):
    tag_id = search_tag(tag)
    if tag_id == None:
        tag_id = create_tag(tag)
        return tag_id
    return tag_id
                        
### ------------------------ api call ---------------------------------------

def api_call(url, tag_id):
    
    category = "MAINSTREAM"
    r = requests.post("http://urldb.empello.net/api/testurls/", data={'url': url,
                                                          'category': category,
                                                          'tags': [tag_id]})
    #print (r.reason) # for debuging

### ------------------------ MAIN ---------------------------------------

def main():
    driver = chrome_setup()
    countries_list = available_countries()
    #countries_list = ["uk"] # for debuging 
    print (f"Making lists for the following countries; {countries_list}")
    not_wanted_list = get_not_wanted_list()
    main_dictionary = set_up(countries_list) 
    for country_code, keywords_list in main_dictionary.items():
        file_name = create_filename(country_code)        
        full_list = get_full_lists(keywords_list, driver, country_code)
        full_clean_list = clean_list(full_list, not_wanted_list)
        write_urls(full_clean_list, country_code, file_name)
    driver.quit()
    #print (main_dictionary)


if __name__ == "__main__":
    main()